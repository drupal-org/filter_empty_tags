<?php

namespace Drupal\filter_empty_tags\Plugin\Filter;

use Drupal\filter\Annotation\Filter;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;

/**
 * @Filter(
 *   id = "filter_empty_tags",
 *   title = @Translation("Filter Empty Tags"),
 *   description = @Translation("Recursively remove empty tags."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_IRREVERSIBLE,
 * )
 */
class FilterEmptyTags extends FilterBase {

  /**
   * Define list of tags that should never be removed.
   *
   * The tags here are meaningful even if they are empty.
   *
   * @var string[]
   */
  protected $doNotConsiderEmpty = [
    'button',
    'canvas',
    'drupal-media',
    'drupal-entity',
    'iframe',
    'object',
    'script',
    'svg',
    'textarea',
    'td',
    'th',
  ];

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {

    // A tag is considered empty if it contains only whitespace,
    // non-breaking spaces, and break tags.
    $processed = $text;
    $matches = [];
    preg_match_all(
      '/<([^<\/>)]*)\b[^>]*>((\s|\xc2\xa0|&nbsp;|<br>|<br\/>|<br \/>)*?)<\/\1>/imsU',
      $text,
      $matches,
    );

    foreach ($matches[1] as $key => $tag) {
      if (!in_array($tag, $this->doNotConsiderEmpty, TRUE)) {
        $processed = str_replace($matches[0][$key], '', $processed);
      }
    }

    // If str_replace made any changes, we want to process again to handle
    // recursive structures of empty tags. This allows for more complicated
    // patterns than using the regex (?R) construct.
    if ($processed === $text) {
      return new FilterProcessResult($text);
    }
    else {
      return $this->process($processed, $langcode);
    }
  }

}
