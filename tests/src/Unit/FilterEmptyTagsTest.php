<?php

namespace Drupal\Tests\filter_empty_tags\Unit;

use Drupal\Core\Language\Language;
use Drupal\filter_empty_tags\Plugin\Filter\FilterEmptyTags;
use Drupal\Tests\UnitTestCase;

/**
 * @coversDefaultClass \Drupal\filter_empty_tags\Plugin\filter\FilterEmptyTags
 * @group filter_empty_tags
 */
class FilterEmptyTagsTest extends UnitTestCase {

  /**
   * @var \Drupal\filter_empty_tags\Plugin\filter\FilterEmptyTags
   */
  protected $filter;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->filter = new FilterEmptyTags([], 'filter_empty_tags', ['provider' => 'test']);
    $this->filter->setStringTranslation($this->getStringTranslationStub());
  }

  /**
   * @covers ::process
   *
   * @dataProvider providerTestFilterEmptyTags
   *
   * @param string $input
   *   Input HTML.
   * @param string $expected_output
   *   The expected output string.
   */
  public function testFilterEmptyTags($input, $expected_output) {
    $this->assertSame($expected_output, $this->filter->process($input, Language::LANGCODE_NOT_SPECIFIED)->getProcessedText());
  }

  /**
   * Provides data for testFilterEmptyTags.
   *
   * @return array
   *   An array of test data.
   */
  public function providerTestFilterEmptyTags() {
    return [
      ['<p></p>', ''],
      ['<div></div>', ''],
      ['<h2></h2>', ''],
      ['<anything></anything>', ''],
      ['<div><p></p></div>', ''],
      ['<div><p></p><ul></ul></div>', ''],
      ['<div>Something<div></div>else</div>', '<div>Somethingelse</div>'],
      ['<h1>this<h2><h3>is</h3><h4></h4></h2></h1>drupal', '<h1>this<h2><h3>is</h3></h2></h1>drupal'],
      ['<p class="something"></p>', ''],
      ['<p id="whatever" class="something"></p>', ''],
      ['<p> </p>', ''],
      ['<p>       </p>', ''],
      ['<p><br/></p>', ''],
      ['<p>&nbsp;</p>', ''],
      ['<p>&nbsp;&nbsp;</p>', ''],
      ['<p>&nbsp; \n &nbsp;</p>', ''],
      ['<p> </p>', ''],
      ["<p>\n</p>", ''],
      ['<p> <b> </b> </p>', ''],
      ['<p>&nbsp;<b>&nbsp;</b>&nbsp;</p>', ''],
      ['<iframe src="https://nasa.gov"></iframe><p></p>', '<iframe src="https://nasa.gov"></iframe>'],
    ];
  }

  /**
   * Test that the filter does not filter non-empty tags.
   *
   * @covers ::process
   *
   * @dataProvider providerTestControlStrings
   *
   * @param string $control
   *   A string that is not expected to change.
   */
  public function testFilterControlStrings($control) {
    $this->assertSame($control, $this->filter->process($control, Language::LANGCODE_NOT_SPECIFIED)->getProcessedText());
  }

  /**
   * Provides data for testFilterControlStrings.
   *
   * @return array
   *   An array of test data.
   */
  public function providerTestControlStrings() {
    return [
      ['<h2>This is a control string</h2><div>This string should <b><em>not</em></b> be altered in ANY&nbsp;WAY by the filter. </div> <hr><p class="some-class"><><><></p>&nbsp;'],
      ['<p> This is not empty </p>'],
      ['<p> <b> hello </b> </p>'],
      ['<h1> <h2> hi <p><div>yes</div></p><br></h2>&nbsp;'],
      ['<p><p>'],
      ['<p><hr></p>'],
      // Test some patterns with "empty" tags we don't consider empty.
      ['<iframe src="https://nasa.gov"></iframe>'],
      ['<table><thead><tr><th>The table header</th><th></th></tr></thead><tbody><tr><td>The table body</td><td></td></tr></tbody></table>'],
      ['<drupal-media data-caption="baz" data-entity-type="media" data-entity-uuid="abc123"></drupal-media>'],
    ];
  }

}
